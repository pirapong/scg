import { Component, OnInit } from '@angular/core';
declare let google: any;
@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.css']
})
export class RestaurantComponent implements OnInit {
  map;
  service;
  infowindow;
  lat: number = 13.828253;
  lng: number = 100.52845070000001;
  data = [];
  constructor() { }

  ngOnInit() {
    this.initAutocomplete()
    this.getRestaurant(this.lat, this.lng)
  }
  addlist(restaurant) {
    this.data = restaurant
  }

  getRestaurant(lat, lng) {
    let pyrmont = new google.maps.LatLng(lat, lng);

    this.map = new google.maps.Map(document.getElementById('map'), {
      center: pyrmont,
      zoom: 15
    });

    let request = {
      location: pyrmont,
      radius: '500',
      type: ['restaurant']
    };
    this.service = new google.maps.places.PlacesService(this.map);
    this.service.nearbySearch(request, (results, status) => {
      let restaurant = [];
      if (status == google.maps.places.PlacesServiceStatus.OK) {
        results.forEach(datas => {
          let locations
          let locationslatlng

          if (datas.photos == null) {
            locations = '';
          } else {
            locations = datas.photos[0].html_attributions[0].split('<a href="')[1].split('">')[0]
          }

          if (!datas.geometry) {
            alert("Returned place contains no geometry");
            return;
          }
          else {
            locationslatlng = datas.geometry.location.lat() + ',' + datas.geometry.location.lng()
          }
          restaurant.push({ icon: datas.icon, name: datas.name, location: locations, locationslatlng: locationslatlng, vicinity: datas.vicinity })
        });
        this.addlist(restaurant)
      }
    });
  }

  initAutocomplete() {
    let map = new google.maps.Map(document.getElementById('map'), {
      center: { lat: 13.828253, lng: 100.52845070000001 },
      zoom: 13,
      mapTypeId: 'roadmap'
    });

    let input = document.getElementById('pac-input');
    let searchBox = new google.maps.places.SearchBox(input);

    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    searchBox.addListener('places_changed', () => {
      let places = searchBox.getPlaces();

      if (places.length == 0) {
        alert("Returned place contains no geometry");
        return;
      } else {
        if (!places[0].geometry) {
          alert("Returned place contains no geometry");
          return;
        }
        else {
          this.lat = places[0].geometry.location.lat();
          this.lng = places[0].geometry.location.lng();
          this.getRestaurant(this.lat, this.lng)
        }
      }
    });
  }

}
